package maxmind

import (
	"context"
	"encoding/json"
	"net/http"
)

type API struct {
	doFunc     func(ctx context.Context, req *http.Request) (*http.Response, error)
	userID     string
	licenseKey string
}

func New(userID, licenseKey string) *API {
	API := &API{
		userID:     userID,
		licenseKey: licenseKey,
	}
	return WithClient(API, http.DefaultClient)
}

func WithClient(a *API, client *http.Client) *API {
	return WithClientFunc(a, wrap(client.Do))
}

func WithClientFunc(a *API, ctxFunc func(context.Context, *http.Request) (*http.Response, error)) *API {
	return &API{
		doFunc:     ctxFunc,
		userID:     a.userID,
		licenseKey: a.licenseKey,
	}
}

func wrap(doFunc func(*http.Request) (*http.Response, error)) func(context.Context, *http.Request) (*http.Response, error) {
	return func(ctx context.Context, req *http.Request) (*http.Response, error) {
		return doFunc(req)
	}
}

func (a *API) Country(ctx context.Context, ipAddress string) (Response, error) {
	return a.fetch(ctx, "https://geoip.maxmind.com/geoip/v2.1/country/", ipAddress)
}

func (a *API) City(ctx context.Context, ipAddress string) (Response, error) {
	return a.fetch(ctx, "https://geoip.maxmind.com/geoip/v2.1/city/", ipAddress)
}

func (a *API) Insights(ctx context.Context, ipAddress string) (Response, error) {
	return a.fetch(ctx, "https://geoip.maxmind.com/geoip/v2.1/insights/", ipAddress)
}

func (a *API) fetch(ctx context.Context, prefix, ipAddress string) (Response, error) {
	req, err := http.NewRequest("GET", prefix+ipAddress, nil)
	if err != nil {
		return Response{}, err
	}

	// authorize the request
	// http://dev.maxmind.com/geoip/geoip2/web-services/#Authorization
	req.SetBasicAuth(a.userID, a.licenseKey)

	// execute the request
	if ctx == nil {
		ctx = context.Background()
	}
	resp, err := a.doFunc(ctx, req)
	if err != nil {
		return Response{}, err
	}
	defer resp.Body.Close()

	// handle errors that may occur
	// http://dev.maxmind.com/geoip/geoip2/web-services/#Response_Headers
	if resp.StatusCode >= 400 && resp.StatusCode < 600 {
		v := Error{}
		err := json.NewDecoder(resp.Body).Decode(&v)
		if err != nil {
			return Response{}, err
		}

		return Response{}, v
	}

	// parse the response body
	// http://dev.maxmind.com/geoip/geoip2/web-services/#Response_Body
	response := Response{}
	err = json.NewDecoder(resp.Body).Decode(&response)
	return response, err
}
